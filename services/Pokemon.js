const { ObjectID } = require('bson');
var Pokemon = require('../schema/pokemon')

async function getAll() {
    try {
        const data = await Pokemon.find().sort({ id: 1 });
        return { count: data.length, data: data }
    } catch (err) {
        console.error(`Error while getting pokemons `, err.message);
        next(err);
    }
}

async function getById(pokemonId) {
    try {
        const filter = { id: pokemonId };
        return await Pokemon.findOne(filter);
    } catch (error) {
        console.error(`Error while getting a pokemon `, err.message);
        next(err);
    }
}

async function getAllOwnedPokemon() {
    try {
        const filter = { owned: true };
        return await Pokemon.find(filter).sort({ id: 1 });
    } catch (error) {
        console.error(`Error while getting a pokemon `, err.message);
        next(err);
    }
}

async function insert(newPokemon) {
    try {
        let message = 'Pokemon inserted successfully';
        if (!Array.isArray(newPokemon)) {
            const pokemons = [newPokemon];
            await Pokemon.insertMany(pokemons);
            return { message };
        }
        await Pokemon.insertMany(newPokemon);
        return { message };
    } catch (err) {
        console.error(`Error while inserting pokemons `, err.message);
        next(err);
    }
}

async function update(pokemonId, pokemon) {
    try {
        const filter = { id: pokemonId }
        const updatedPokemon = {
            $set: pokemon
        }
        const result = await Pokemon.updateOne(filter, updatedPokemon)
        let message = "Pokemon updated successfully"
        return { message };
    } catch (err) {
        console.error(`Error while updating pokemons `, err.message);
        next(err);
    }
}

async function remove(pokemonId) {
    try {
        const filter = { id: pokemonId }
        const result = await Pokemon.remove(filter)
        let message = "Pokemon deleted successfully"
        return { message };
    } catch (err) {
        console.error(`Error while deleting pokemon `, err.message);
        next(err);
    }
}

async function catchOnePokemon(pokemonId) {
    try {
        //Find pokemon by ignored case name
        let pokemon = await Pokemon.findOne({ id: pokemonId })
        const pokemonName = pokemon.name
        let message = "Failed to catch a " + pokemonName
        let succeed = false;

        //If succeed catching the pokemon
        if (Math.random() < 0.5) {
            message = pokemonName + " was Caught !"
            succeed = true
            //If never owned the pokemon
            if (!pokemon.owned) {
                pokemon.owned = true;
            }
            pokemon.ownedAmount += 1;
            pokemon.save()
        }
        return {
            message: message,
            succeed: succeed,
            data: pokemon
        }
    } catch (err) {
        console.error(`Error while catching pokemon `, err.message);
        next(err);
    }
}

async function releaseOnePokemon(pokemonId) {
    try {
         //Find pokemon by ignored case name
    let pokemon = await Pokemon.findOne({ id: pokemonId })
    const pokemonName = pokemon.name
    let message = pokemonName + " was release outside. Bye " + pokemonName
    //Check if user have pokemon
    if (!pokemon.owned) {
        return { message: "You don't have a single " + pokemonName, succeed: false }
    }

    //Pick random number between 0-100
    let chanceNum = Math.floor(Math.random() * 100);

    //Check number if prime
    for (let i = 2; i <= Math.sqrt(chanceNum); i++) {
        if (chanceNum % i == 0) {
            message = "You're too sad to release " + pokemonName + " :( . Release failed.";
            succeed = false
            return { message, chanceNum, succeed: false }
        }
    }
    //Check if owned amount is 1 then change owned to none and owned amount to 0
    if (pokemon.ownedAmount == 1) {
        pokemon.owned = false
    }
    pokemon.ownedAmount -= 1;
    pokemon.save()
    return { message, succeed: true, chance: chanceNum, data: pokemon }
    } catch (err) {
        console.error(`Error while catching pokemon `, err.message);
        next(err);
    }
   
}

async function renameOnePokemon(pokemonId) {
    try {
        //Find pokemon by Pokemon ID not document _id
        let pokemon = await Pokemon.findOne({ id: pokemonId })
        //Check if pokemon name has '-' then initialize '-0'
        if (pokemon.name.indexOf('-') < 0) {
            pokemon.name = pokemon.name + "-0"
            pokemon.save()
        } else {
            let pokeNameArr = pokemon.name.split('-')
            let currNum = parseInt(pokeNameArr[1])

            //Check if name number is 0 and previous fibonacci number is 0 
            // then initilize name number to 1 
            // else add name number and previous fibonacci number
            if (parseInt(pokeNameArr[1]) == 0 && pokemon.prevFiboNum == 0) {
                currNum = 1
            } else {
                let temp = currNum;
                currNum += pokemon.prevFiboNum;
                pokemon.prevFiboNum = temp;
            }
            pokeNameArr[1] = currNum
            let renamedName = pokeNameArr.join('-')
            pokemon.name = renamedName;
            pokemon.save()
        }
        return { message: "Pokemon has been renamed", succeed: true, data: pokemon }
    } catch (err) {
        console.error(`Error while catching pokemon `, err.message);
        next(err);
    }

}

module.exports = {
    getAll,
    getById,
    getAllOwnedPokemon,
    insert,
    update,
    remove,
    catchOnePokemon,
    releaseOnePokemon,
    renameOnePokemon
    // uploadFile
}