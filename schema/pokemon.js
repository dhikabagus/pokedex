const mongoose = require("../config/database");

const pokemonSchema = new mongoose.Schema({
  id:Number,
  name:String,
  species: {
    name: String,
    url: String
  },
  moves: [
    {
      move: {
        name: String,
        url: String
      }
    }
  ],
  types: [
    {
      slot:Number,
      type: {type:Object},
    }
  ],
  picture:{
    name:String,
    url:String
  },
  owned:{
    type:Boolean,
    default:false
  },
  ownedAmount:{
    type:Number,
    default:0
  },
  prevFiboNum:{
    type:Number,
    default:0
  }
});

module.exports = mongoose.model("pokemon", pokemonSchema);