# Pokedex App 
## By : Dhika Bagus Danindra
## BackEnd : Express.js
## Database : NoSql

## How to run App : 
1. npm install
2. copy and edit .env file
3. npm run start
4. import postman collection if needed

## Pokedex Backend App Listening at http://localhost:3000

## Database
Database URI in ".env.example" file is open for public. Or setup local MongoDB are also welcome, simply change MONGO_URI in ".env" file and Pokedex App is good to go


