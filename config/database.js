const mongoose = require('mongoose');
require("dotenv").config();
const { MONGO_URI} = process.env

// Database connection
mongoose.connect(MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
},(error,result)=>{
  if(error){
        return console.log("Database failed to connect")
      }
  console.log("Database Connected to : " + MONGO_URI)
});

module.exports = mongoose;