var express = require('express')
var router = express.Router()
var pokemon = require('../services/Pokemon')
const multer = require('multer')

const diskStorage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, "../frontend-pokedex/src/assets/");
    },
    filename: function (req, file, cb) {
      cb(
        null,
        file.originalname
      );
    },
  });

/* GET All Pokemons. */
router.get('/', async function (req, res, next) {
    try {
        res.json(await pokemon.getAll())
    } catch (err) {
        console.error(`Error while getting pokemons `, err.message);
        next(err);
    }
});

/* GET All Owned Pokemon*/
router.get('/mypokemon/owned', async function (req, res, next) {
    try {
        res.json({ data: await pokemon.getAllOwnedPokemon() })
    } catch (err) {
        console.error(`Error while getting all owned pokemons `, err.message);
        next(err);
    }
})

/* GET Pokemon By Id. */
router.get('/:id', async function (req, res, next) {
    try {
        res.json(await pokemon.getById(req.params.id))
    } catch (err) {
        console.error(`Error while getting a pokemon `, err.message);
        next(err);
    }
});



/* POST a Pokemon. */
router.post('/', async function (req, res, next) {
    try {
        res.json(await pokemon.insert(req.body))
    } catch (err) {
        console.error(`Error while getting pokemons `, err.message);
        next(err);
    }
});

/* PUT a Pokemon. */
router.put('/:id', async function (req, res, next) {
    try {
        res.json(await pokemon.update(req.params.id, req.body))
    } catch (err) {
        console.error(`Error while getting pokemons `, err.message);
        next(err);
    }
});

/* DELETE a Pokemon. */
router.delete('/:name', async function (req, res, next) {
    try {
        res.json(await pokemon.remove(req.params.name))
    } catch (err) {
        console.error(`Error while getting pokemons `, err.message);
        next(err);
    }
});

/* Catch a Pokemon. */
router.get('/catch/:name', async function (req, res, next) {
    try {
        res.json(await pokemon.catchOnePokemon(req.params.name))
    } catch (err) {
        console.error(`Error while getting pokemons `, err.message);
        next(err);
    }
});

/* Release a Pokemon. */
router.get('/release/:name', async function (req, res, next) {
    try {
        res.json(await pokemon.releaseOnePokemon(req.params.name))
    } catch (err) {
        console.error(`Error while getting pokemons `, err.message);
        next(err);
    }
});

/* Release a Pokemon. */
router.get('/rename/:pokemonId', async function (req, res, next) {
    try {
        res.json(await pokemon.renameOnePokemon(req.params.pokemonId))
    } catch (err) {
        console.error(`Error while getting pokemons `, err.message);
        next(err);
    }
});



/* Upload file */
router.post('/uploadFile', multer({ storage: diskStorage }).single("image"), async function (req, res, next) {
    try {
        console.log(req.file)
        res.json({file:req.file})
    } catch (err) {
        console.error(`Error while uploading file `, err.message);
        next(err);
    }
})

module.exports = router;