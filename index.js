require("dotenv").config();
var express = require('express')
var app = express();
const { API_PORT} = process.env;
var bodyParser = require("body-parser");
var cors = require('cors')

//Routes
var pokemonRoute = require('./routes/pokemonRoute')

//express setup
app.use(express.json());
app.use(
    express.urlencoded({
        extended: true,
    })
);
app.use(bodyParser.json());
app.use(cors())

//express route setup
app.use('/api/v1/pokemons', pokemonRoute)

/* Error handler middleware */
app.use((err, req, res, next) => {
    const statusCode = err.statusCode || 500;
    console.error(err.message, err.stack);
    res.status(statusCode).json({ message: err.message });
    return;
});

app.get('/',(req,res) =>{
    res.json({message:"Welcome to pokedex app"})
})

app.listen(API_PORT, () => {
    console.log(`Pokedex App listening at http://localhost:${API_PORT}`);
    }
)